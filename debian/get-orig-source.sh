#! /bin/sh
set -e

if [ -z "$DIR" ]; then
	DIR=libg3d
fi

# try to download source package
if [ "$1" != "snapshot" ]; then
	uscan --verbose --force-download
else
	umask 000
	TMP="`mktemp -t -d`"
	MANIFEST="`mktemp -t`"
	R="$(svn info "svn://svn.gna.org/svn/${DIR}/trunk"|grep '^Revision'| sed 's/^Revision: //')"
	VERSION=$(dpkg-parsechangelog | sed -ne 's/^Version: \(\([0-9]\+\):\)\?\(.*\)-.*/\3/p')
	REV="${VERSION}+svn${R}"
	svn export -r"${R}" "svn://svn.gna.org/svn/${DIR}/trunk" "${TMP}/${DIR}-${REV}/"
	rm -rf "${TMP}/${DIR}-${REV}/debian"
	TARNAME="${DIR}_${REV}.orig.tar"
	(
		cd "${TMP}"
		find "${DIR}-${REV}/" -type f |sed 's/^\.*\/*//'|sort > "$MANIFEST"
		tar cf "${TARNAME}" --owner 0 --group 0 --numeric-owner --no-recursion --files-from "$MANIFEST"
	)
	mv "${TMP}/${TARNAME}" .
	gzip -n -f "${TARNAME}"
	rm -rf "${TMP}" "$MANIFEST"
fi
