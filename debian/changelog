libg3d (0.0.8-32) UNRELEASED; urgency=medium

  * Drop duplicated fields from upstream/metadata
  * Update debian/watch for format 4
  * Ignore the not-install *.la and development helper files
  * debian/control:
    - Switch to debhelper compat 13
    - Upgraded to policy 4.6.0, no changes required
    - Switch to correct DEP-14 unstable branch name
    - Remove versioned constraints unnecessary since buster
  * debian/rules:
    - Drop default flag --as-needed from linker flags

 -- Sven Eckelmann <sven@narfation.org>  Wed, 29 Apr 2020 15:04:57 +0200

libg3d (0.0.8-31) unstable; urgency=medium

  * debian/control:
    - Switch debhelper dependency to debhelper-compat
    - Upgraded to policy 4.5.0, no changes required
    - Allow build without (fake)root
    - Move repository to from ecsv-guest to ecsv salsa user
  * debian/libg3d0.symbols:
    - Add Build-Depends-Package to get proper dependencies when a package
      build-depends on a higher version of libg3d-dev than what it gets
      from the used symbols.
  * debian/copyright
    - Update copyright years to 2020
  * debian/patches:
    - Add Switch-libxml2-lookup-from-xml2-config-to-pkg-config.patch,
      Switch libxml2 lookup from xml2-config to pkg-config (Closes: #949522)

 -- Sven Eckelmann <sven@narfation.org>  Tue, 21 Jan 2020 19:17:05 +0100

libg3d (0.0.8-30) unstable; urgency=medium

  * Upgraded to policy 4.4.0, no changes required
  * Use /usr/share/doc/ hierarchy for libg3d-docs' doc-base

 -- Sven Eckelmann <sven@narfation.org>  Sun, 28 Jul 2019 12:07:13 +0200

libg3d (0.0.8-29) unstable; urgency=medium

  * Upload to unstable

 -- Sven Eckelmann <sven@narfation.org>  Sat, 06 Jul 2019 15:40:58 +0200

libg3d (0.0.8-28) experimental; urgency=medium

  * debian/control:
    - Automatically select correct Vcs-Git branch
  * debian/rules:
    - Drop out-of-tree autoreconf which broke new debhelper autoreconf on
      second build (Closes: #924756)

 -- Sven Eckelmann <sven@narfation.org>  Sun, 17 Mar 2019 08:26:15 +0100

libg3d (0.0.8-27) experimental; urgency=medium

  * debian/control
    - Upgraded to policy 4.3.0, no changes required
    - Switch to debhelper 12
  * debian/copyright
    - Update copyright years to 2019

 -- Sven Eckelmann <sven@narfation.org>  Sat, 02 Mar 2019 20:16:55 +0100

libg3d (0.0.8-26) unstable; urgency=medium

  * debian/control:
    - Add Multi-Arch tags to packages without maintainer scripts and equal
      common files
  * Disable false positive lintian error about libc-less plugins

 -- Sven Eckelmann <sven@narfation.org>  Sat, 20 Oct 2018 07:35:03 +0200

libg3d (0.0.8-25) unstable; urgency=medium

  * Upgraded to policy 4.2.1
    - remove get-orig-source rule from debian/rules
  * debian/rules
    - Drop (now default) parameter --parallel for dch
  * debian/control:
    - update to debhelper 10
    - drop dependency to dh-autoreconf
    - Move VCS-* to salsa.debian.org
  * Switch upstream URLs to github.com

 -- Sven Eckelmann <sven@narfation.org>  Thu, 18 Oct 2018 19:59:23 +0200

libg3d (0.0.8-24) unstable; urgency=medium

  * run dh_auto_configure for both arch and indep packages to make sure that
    documentation is build during arch while the indep packages aren't build
    with documentation (Closes: #883882)

 -- Sven Eckelmann <sven@narfation.org>  Sat, 09 Dec 2017 08:33:03 +0100

libg3d (0.0.8-23) unstable; urgency=medium

  [ Sven Eckelmann ]
  * Fix lintian overrides on i386
  * Upgraded to policy 4.1.2, no changes required
  * debian/copyright:
    - Update copyright years
  * debian/rules
    - Remove ddeb migration conflict against pre-stretch package
    - only run dh_autoreconf once

  [ Helmut Grohne ]
  * Fix FTCBFS: Do not build documentation during arch-only. (Closes: #883882)

 -- Sven Eckelmann <sven@narfation.org>  Sat, 09 Dec 2017 00:37:42 +0100

libg3d (0.0.8-22) unstable; urgency=medium

  * debian/copyright:
    - Update copyright years
    - Fix LGPL-2.1 full name
  * Override the lintian warning about unfortified functions
  * debian/patches:
    - Add Fix-spelling-errors.patch, Fix spelling errors
  * debian/control
    - Change Vcs-Git and Vcs-Browser to https://
  * Change debian URLs to https://
  * Sort debian control files with `wrap-and-sort -abst`

 -- Sven Eckelmann <sven@narfation.org>  Sun, 31 Jan 2016 17:42:51 +0100

libg3d (0.0.8-21) unstable; urgency=medium

  * Upgraded to policy 3.9.6, no changes required
  * Update years in debian/copyright
  * debian/patches:
    - Move patches to alternative DEP-3 format and manage them with gbp pq
  * Change debian/upstream/metadata to point to libg3d upstream URLs
  * debian/control, debian/rules:
    - Drop libg3d-dbg in favor of -dbgsym package
    - Drop obsolete XS-Testsuite field

 -- Sven Eckelmann <sven@narfation.org>  Wed, 30 Dec 2015 20:13:23 +0100

libg3d (0.0.8-20) unstable; urgency=medium

  * Upload to unstable

 -- Sven Eckelmann <sven@narfation.org>  Sat, 30 Aug 2014 17:37:21 +0200

libg3d (0.0.8-19) experimental; urgency=low

  * debian/watch
    - Verify new upstream versions using GPG key 06FD8C6B46E3D322BAD4
  * debian/copyright:
    - Update e-mail address of Marek Lindner
    - Add Simon Wunderlich
  * Upgraded to policy 3.9.5, no changes required
  * Update copyright years in debian/copyright
  * debian/rules:
    - Use Largefile Support enabled C API

 -- Sven Eckelmann <sven@narfation.org>  Sat, 30 Aug 2014 15:56:22 +0200

libg3d (0.0.8-18) experimental; urgency=low

  [ Sven Eckelmann ]
  * Upgraded to policy 3.9.4, no changes required
  * Remove obsolete DM-Upload-Allowed in debian/control

  [ Vibhav Pant ]
  * Add simple autopkgtest to package (Closes: #702424)

 -- Sven Eckelmann <sven@narfation.org>  Wed, 06 Mar 2013 12:38:53 +0100

libg3d (0.0.8-17) unstable; urgency=low

  * Enable all hardening flags in debian/rules
  * Upgraded to policy 3.9.3, no changes required
  * debian/patches:
    - Add implicit_declaration.patch, Fix implicit declarations of
      functions
  * Use buildflags.mk to set security flags in debian/rules
  * Use DEB_LDFLAGS_MAINT_APPEND to add --as-needed linker flag
  * Upgrade debhelper compat to v9

 -- Sven Eckelmann <sven@narfation.org>  Sat, 28 Apr 2012 12:11:26 +0200

libg3d (0.0.8-16) unstable; urgency=low

  * Update copyright years in debian/copyright
  * debian/patches:
    Add pkglib_data.patch, Fix build with automake 1.11.3 (Closes LP: #935226)
  * Disable libmagic support until a workaround is added

 -- Sven Eckelmann <sven@narfation.org>  Sun, 19 Feb 2012 02:27:51 +0100

libg3d (0.0.8-15) unstable; urgency=low

  * Use dh_autoreconf in debian/rules to fix --as-needed bug in libtool
  * Add optional dependencies to libg3d-dev
  * Mark all targets in debian/rules as phony
  * Remove hardening-includes which are now integrated in dpkg-
    buildflags
  * Use debian packaging manual URL as format identifier in
    debian/copyright

 -- Sven Eckelmann <sven@narfation.org>  Mon, 31 Oct 2011 11:31:33 +0100

libg3d (0.0.8-14) unstable; urgency=low

  * Upgraded to policy 3.9.2, no changes required
  * Update Vcs-* fields to new anonscm.debian.org URLs in debian/control
  * Add multiarch support (Closes: #623766)

 -- Sven Eckelmann <sven@narfation.org>  Sat, 11 Jun 2011 16:49:48 +0200

libg3d (0.0.8-13) unstable; urgency=low

  * Upload to unstable
  * Updated my maintainer e-mail address
  * Keep dependencies on separate lines in debian/control
  * debian/copyright:
    - Update to DEP5 revision 164
    - Update copyright years

 -- Sven Eckelmann <sven@narfation.org>  Sun, 06 Feb 2011 11:52:28 +0100

libg3d (0.0.8-12) experimental; urgency=low

  * Upgrade debhelper compat to v8
  * set *FLAGS using dpkg-buildflags in debian/rules to work like
    dpkg-buildpackage when called directly
  * Use hardening-includes for CFLAGS and LDFLAGS hardened builds
  * Explicit build depend on pkg-config

 -- Sven Eckelmann <sven@narfation.org>  Thu, 23 Sep 2010 13:37:09 +0200

libg3d (0.0.8-11) unstable; urgency=low

  * Upgraded to policy 3.9.1, no changes required
  * debian/patches:
    - Add fix-memory-leaks-3.patch, Fix memory leaks in core (Closes:
      #593209)
    - Add fix-memory-leaks-dae.patch, Fix memory leaks in dae plugin
      (Closes: #593209)

 -- Sven Eckelmann <sven@narfation.org>  Mon, 16 Aug 2010 21:29:40 +0200

libg3d (0.0.8-10) unstable; urgency=low

  * debian/patches:
    - Add fix-memory-leaks-2.patch, Fix memory leaks (Closes: #570084)
      Thanks Picca Frederic-Emmanuel <picca@synchrotron-soleil.fr>
    - Add pc-glib2-private.patch, Only require glib2.0 for header files
      when using pkg-config
  * Remove outdated README.source
  * debian/rules:
    - Enable parallel builds using dh's --parallel
    - Inform about missing installed files using dh's --list-missing
  * Upgraded to policy 3.9.0, no changes required

 -- Sven Eckelmann <sven@narfation.org>  Mon, 28 Jun 2010 23:51:12 +0200

libg3d (0.0.8-9) unstable; urgency=low

  * Correct spelling errors found by lintian
  * debian/copyright: Update copyright years
  * Upgraded to policy 3.8.4, no changes required
  * debian/patches:
    - Add load-so-modules.patch, Identify modules by the ending .so
      instead of .la (Closes: #568797)

 -- Sven Eckelmann <sven@narfation.org>  Mon, 08 Feb 2010 10:39:30 +0100

libg3d (0.0.8-8) unstable; urgency=low

  * Register HTML documentation in doc-base
  * debian/patches:
    - Add configure-cxx.patch, Add AC_PROG_CXX for tests/Makefile.am
    - Add autoreconf-libtool.patch, Allow libtool to refresh its own
      files to keep them consistent
    - Remove distclean-xml.patch, Distclean isn't run anymore in clean
      target
    - Remove link-as-needed.patch, ltmain.sh is replaced by Debian's
      ltmain.sh
  * Recreate autotool generated files before configure
  * Build html gtk-docs and don't use version included in orig.tar.gz
  * Don't install .la files anymore, there are no .la files which
    references them, but they are known to cause unnecessary
    dependencies

 -- Sven Eckelmann <sven@narfation.org>  Sat, 26 Dec 2009 11:08:28 +0100

libg3d (0.0.8-6) unstable; urgency=low

  * Convert to 3.0 (quilt) source format
  * debian/control:
    - Remove unneeded build dependency to quilt
    - Build-depend on autotools-dev to allow cdbs to update config.sub
      and config.guess
    - Remove cdbs and change debhelper dependency to 7 for for dh
      support
  * Rewrote debian/rules with dh
  * debian/patches:
    - Add distclean-xml.patch, Don't remove shipped xml files when
      calling distclean

 -- Sven Eckelmann <sven@narfation.org>  Mon, 16 Nov 2009 01:31:39 +0100

libg3d (0.0.8-5) unstable; urgency=low

  * Reduce debhelper dependency to 5
  * Re-enable filetype detection using libmagic
  * Convert debian/copyright to new dep5 version
  * Upgraded to policy 3.8.3, no changes required
  * Add README.source with information about patch management
  * debian/patches:
    - Change to dep3 patch tagging guidelines
    - Remove number before patches as order is given by
      debian/patches/series
  * Remove shlibs:Depends for binary packages without shared libs
    dependencies

 -- Sven Eckelmann <sven@narfation.org>  Fri, 30 Oct 2009 17:58:34 +0100

libg3d (0.0.8-4) unstable; urgency=low

  * Correct changelog entry for libg3d-0.0.8-3

 -- Sven Eckelmann <sven@narfation.org>  Sat, 09 May 2009 17:00:06 +0200

libg3d (0.0.8-3) unstable; urgency=low

  * debian/rules:
    - Rewrote using cdbs
    - Only make disappearing of symbols a fatal error for dh_makeshlibs
  * debian/control:
    - Move Vcs-* to git.debian.org
    - Move libg3d-dbg to new section debug
    - Upgraded to policy 3.8.1, no changes required
    - Allow Debian Maintainers to upload new versions of libg3d
  * debian/patches:
    - Add 102-endless-seek.patch to fix possible infinite loop in
      g3d_stream_skip

 -- Sven Eckelmann <sven@narfation.org>  Wed, 06 May 2009 13:15:31 +0200

libg3d (0.0.8-2) unstable; urgency=low

  * Add package libg3d-dbg with debug symbols
  * Shorten copied descriptions in debian/control
  * debian/rules:
    - Run configure only once for both binary-indep and binary-arch
    - Use quilt to manage patches
  * debian/patches:
    - Add 100-fix-memory-leaks.patch, fix memory leaks of plugins
    - Add 101-link-as-needed.patch, reduce unneeded linking against
      libraries

 -- Sven Eckelmann <sven@narfation.org>  Sun, 15 Feb 2009 11:38:53 +0100

libg3d (0.0.8-1) experimental; urgency=low

  * New Upstream Version

 -- Sven Eckelmann <sven@narfation.org>  Mon, 19 Jan 2009 11:16:45 +0100

libg3d (0.0.7-1) experimental; urgency=low

  * New Upstream Version
  * debian/copyright:
    - Add new authors
    - Add email address of Oliver Dippel
  * Remove upstream merged patches/100-read-float-on-big-endian.patch
  * debian/control:
    - Remove quilt dependency
    - Build-Depend on libgsf-(gnome-)1-dev for new plugins
    - Build-Depend on libz-dev for deflated streams support
    - Add COLLADA, ASE, LDraw and KMZ to supported format list
  * debian/rules:
    - Compile outside the source directory
    - Use precompiled api docs instead of recompiling it again
    - Allow to disable gcc optimisations by using DEB_BUILD_OPTIONS
      noopt
    - Allow parallels builds by using DEB_BUILD_OPTIONS parallel=n
    - Replace dh_clean -k with dh_prep
    - Remove top level -stamp files with dh_clean (>= 7)
  * Provide symbol file for libg3d0 and fail if it doesn't match with
    libg3d0 symbols

 -- Sven Eckelmann <sven@narfation.org>  Fri, 16 Jan 2009 19:15:31 +0100

libg3d (0.0.6-6) unstable; urgency=low

  * Remove Timo Schneider as Maintainer and change it to myself
    (Closes: #507390)

 -- Sven Eckelmann <sven@narfation.org>  Mon, 01 Dec 2008 00:36:03 +0100

libg3d (0.0.6-5) unstable; urgency=low

  * Replace libg3d@packages.debian.org maintainer with first Uploader in
    debian/control
  * Build depend on flex to fix warning of libg3d plugin loader
  * Add additional information to 100-read-float-on-big-endian.patch

 -- Sven Eckelmann <sven@narfation.org>  Sat, 29 Nov 2008 20:17:59 +0100

libg3d (0.0.6-4) unstable; urgency=low

  * Remove symbol file for libg3d0
  * Don't fail if symbol file doesn't match with libg3d0 symbols or
    symbol file was not provided

 -- Sven Eckelmann <sven@narfation.org>  Tue, 11 Nov 2008 15:36:59 +0100

libg3d (0.0.6-3) unstable; urgency=low

  * Only depend on libglib2.0-dev in -dev packages
  * Add separate copyright information for debian/* to debian/copyright

 -- Sven Eckelmann <sven@narfation.org>  Wed, 15 Oct 2008 13:57:45 +0200

libg3d (0.0.6-2) unstable; urgency=low

  * Fix lintian warnings
    - Only call distclean when a makefile is present
    - Add standard depends to fix missing depend on libc
    - Provide symbol file for libg3d0 and fail if it doesn't match with
      libg3d0 symbols
    - Add watch file
  * Promote libg3d-plugins to "Recommends" for libg3d0
  * Include /usr/share/quilt/quilt.make in debian/rules to manage
    patches and add quilt (>= 0.40) to Build-Depends.
  * Fix reading of filetypes with floats on big endian systems
    (Closes: #501809)
  * Add homepage to control file
  * Move copyright to new copyright format
  * Add Vcs information to debian/control
  * Add myself to Uploaders list as discussed with Timo Schneider

 -- Sven Eckelmann <sven@narfation.org>  Fri, 10 Oct 2008 18:49:09 +0200

libg3d (0.0.6-1) unstable; urgency=low

  * initial release

 -- Timo Schneider <timo.schneider@s2004.tu-chemnitz.de>  Tue, 19 Dec 2006 01:04:31 +0100
